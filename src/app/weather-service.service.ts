/* Steps 
 * Import HTTPService
 * Inject HTTPService into constructor (shorthand syntax also viable)
 */

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class WeatherService {

	constructor(private _http: Http) {
	}

	getLocationData(searchQuery) {
		return this._http.get('http://interview.toumetisanalytics.com/location/' + searchQuery)
			.map((response: Response) => response.json());
    }
    getWeatherData(woeid) {
    	return this._http.get('http://interview.toumetisanalytics.com/weather/' + woeid)
			.map((response: Response) => response.json());
	}

}
