import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.sass']
})
export class CurrentWeatherComponent implements OnInit {

  title:string;
  @Input() weather;

  constructor() { 
    this.title = "Current Weather";
  }

  ngOnInit() {
  }

}
