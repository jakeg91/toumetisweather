import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.sass']
})
export class TemperatureComponent implements OnInit {

  title:string;
  @Input() weather;
  
  constructor() { 
  	this.title = "Current Temperature";
  }

  ngOnInit() {
  }

}
