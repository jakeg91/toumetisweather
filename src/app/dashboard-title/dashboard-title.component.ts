import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-title',
  templateUrl: './dashboard-title.component.html',
  styleUrls: ['./dashboard-title.component.sass']
})
export class DashboardTitleComponent implements OnInit {

  title:string;
  @Input() weather;

  constructor() { 
  	this.title = "Weather Dashboard"
  }

  ngOnInit() {
  }

}