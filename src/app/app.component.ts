import { Component, OnInit } from '@angular/core';
import { WeatherService } from './weather-service.service';
import { Chart } from 'angular-highcharts';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  
  searchQuery:string = "";
  weatherData:any;
  woeid:any;
  chartTitle:string = 'Six Day Forecast';
  weatherChart:Chart;
  searchError:string;

  //https://stackoverflow.com/questions/39053965/angular2-multiple-dependent-sequential-http-api-calls
  getLocation() {
  	this._weatherService.getLocationData(this.searchQuery)
  		.subscribe( 
  			(location) => this.woeid = location[0].woeid,
  			(error) => this.searchError = error, 
  			() => this.getWeather()
  		);
  }

  getWeather() {
  	this._weatherService.getWeatherData(this.woeid)
  		.subscribe( 
        (weather) => { 
          this.weatherData = weather; 
          //console.log(this.weatherData); 
        },
        (error) => this.searchError = error,
        () => this.generateChart()
      );
  }

  generateChart() {

    //Large Chart
    var catArray = this.generateCategories();
    var minTempSeries = this.getSeries("min_temp");
    var avgTempSeries = this.getSeries("the_temp");
    var maxTempSeries = this.getSeries("max_temp");

    this.weatherChart = new Chart({
      chart: {
          type: 'column'
      },
      title: {
          text: ''
      },
      xAxis: {
          categories: catArray
      },
      yAxis: {
          title: {
              text: 'Values'
          }
      },
      series: [{
          name: 'Min Temp',
          data: minTempSeries
      }, {
          name: 'Avg Temp',
          data: avgTempSeries
      }, {
          name: 'Max Temp',
          data: maxTempSeries
      }]
    })
  }

  generateCategories() {
    var w = this.weatherData.consolidated_weather;
    var dates = [];
    for (var i = 0; i < w.length; i++) {
      dates.push(moment(new Date(w[i].applicable_date)).format("DD. MMM"));
    }
    return dates;
  }

  getSeries(prop) {
    var w = this.weatherData.consolidated_weather;
    var seriesArray = [];
    for (var i = 0; i < w.length; i++) {
      let v = w[i][prop];
      v = Number(v.toFixed(2));
      seriesArray.push(v);
    }
    return seriesArray;
  }

  constructor(private _weatherService: WeatherService) {

  }

  ngOnInit() {
  	
  }
}