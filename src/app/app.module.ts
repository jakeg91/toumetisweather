import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { DashboardTitleComponent } from './dashboard-title/dashboard-title.component';
import { TemperatureComponent } from './temperature/temperature.component';
import { CurrentWeatherComponent } from './current-weather/current-weather.component';
import { SundownComponent } from './sundown/sundown.component';
import { SixDayForecastComponent } from './six-day-forecast/six-day-forecast.component';
import { WeatherService } from './weather-service.service';
import { ChartModule } from 'angular-highcharts';
import { TimeToSunsetPipe } from './time-to-sunset.pipe';

@NgModule({
  declarations: [
    AppComponent,
    DashboardTitleComponent,
    TemperatureComponent,
    CurrentWeatherComponent,
    SundownComponent,
    SixDayForecastComponent,
    TimeToSunsetPipe,
  ],
  imports: [
    BrowserModule, HttpModule, ChartModule
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
