import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'timeToSunset'
})
export class TimeToSunsetPipe implements PipeTransform {

  transform(value: any, args?: any): any {
  	var currentHour = moment().hour();
  	var sunsetHour = moment(new Date(value)).format("HH");
  	var hoursTo = Number(sunsetHour) - Number(currentHour);
    if (hoursTo < 0) {
    	if (Math.abs(hoursTo) == 1) {
			return Math.abs(hoursTo) + " hour ago";
    	} else {
    		return Math.abs(hoursTo) + " hours ago";
    	}
    }  
    if (hoursTo > 0) {
    	if (hoursTo == 1) {
    		return hoursTo + " hour";
    	} else {
    		return hoursTo + " hours";
    	}
    }
    if (hoursTo == 0) {
    	return "Now";
    }
  }

}
