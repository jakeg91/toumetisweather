import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SundownComponent } from './sundown.component';

describe('SundownComponent', () => {
  let component: SundownComponent;
  let fixture: ComponentFixture<SundownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SundownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SundownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
