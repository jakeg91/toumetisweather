import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sundown',
  templateUrl: './sundown.component.html',
  styleUrls: ['./sundown.component.sass']
})
export class SundownComponent implements OnInit {

  title:string;
  @Input() weather;

  constructor() { 
  	this.title = "Sundown";
  }

  ngOnInit() {
  }

}
