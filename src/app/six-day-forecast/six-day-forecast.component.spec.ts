import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SixDayForecastComponent } from './six-day-forecast.component';

describe('SixDayForecastComponent', () => {
  let component: SixDayForecastComponent;
  let fixture: ComponentFixture<SixDayForecastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SixDayForecastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixDayForecastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
