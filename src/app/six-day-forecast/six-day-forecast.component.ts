import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-six-day-forecast',
  templateUrl: './six-day-forecast.component.html',
  styleUrls: ['./six-day-forecast.component.sass']
})
export class SixDayForecastComponent implements OnInit {

  title:string;
  @Input() weather;
  chart:Chart;

  constructor() {
    this.title = "Six Day Forecast";
  }

  ngOnInit() {
  }

}


//Chart type = column
//Title = blank
//X-axis = consolidated weather array loop! Generate this first
//y axis = Title of 'Values'
//Series = Min temp, avg temo & max temp